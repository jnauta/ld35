// restore game data, if any (save & restore functions are at bottom of code)
if(restoreState()){
	var dummyvar = restoreState();
	if(dummyvar['highscore']){
		myhighscore = dummyvar['highscore'];
	}
}

function saveState(state) {
    window.localStorage.setItem("gameState", JSON.stringify(state));
}
 
function restoreState() {
    var state = window.localStorage.getItem("gameState");
    if (state) {
        return JSON.parse(state);
    } else {
        return null;
    }
}
