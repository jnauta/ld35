var bgMusic = null;

// LOADING SCENE
Crafty.scene('Main', function() {


	Crafty.background('#FFFFFF url(assets/images/bgsquare.png) repeat center center'); // 'assets/images/bgslice.png');

	// Crafty.background(mycolors.background);

	Crafty.timer.FPS(80);
	Crafty.viewport.bounds = {
		min: {
			x: 0,
			y: 0
		},
		max: {
			x: Game.width(),
			y: Game.height()
		}
	};

	if (!bgMusic) {
		bgMusic = Crafty.audio.play('bgmusic', -1, 0.1);
		if(mutemusic){
			bgMusic.pause();
		}
	}

	buildLevel = function() {
		Crafty('2D').each(function() {
			if (!this.has("Snake"))
				this.destroy();
		});
		Crafty.viewport.clampToEntities = true;
		info = Crafty.e('Info');
		var level = TileMaps[levels[levelIdx]];
		var tileSets = level.tilesets;
		var xTiles, yTiles;
		for (var i = tileSets.length - 1; i >= 0; i--) {
			var t = tileSets[i];
			if (t.name === "tiles") {
				// these are the real tiles; solid stuff, background etc.
				tileSize = t.tilewidth;
				xTiles = t.imagewidth / t.tilewidth;
				yTiles = t.imageheight / t.tileheight;
			}
		};


		solidLayer = null;
		bgLayer = null;
		tunnelLayer = null;
		domains = new Array();
		sheep = [];

		for (var i = level.layers.length - 1; i >= 0; i--) {
			var layer = level.layers[i];
			if (layer.name === "solid") {
				solidLayer = layer;
			} else if (layer.name === "background") {
				bgLayer = layer;
			} else if (layer.name === "tunnels") {
				tunnelLayer = layer;
			} else if (layer.name === "objects") {

				// have to subtract tileSize, bug in Tiled places tiles and objects differently
				var blackList = ["domain", "temple", "tower", "shepherd", "player"];

				layer.objects.map(function(x) {
					if (blackList.indexOf(x.type) === -1) {
						x.y -= tileSize;
					}
				});
				sheep = layer.objects.filter(function(x) {
					return x.type === "sheep";
				});
				sheep.sort(function(x, y) {
					return x.properties.index > y.properties.index;
				});
				domains = layer.objects.filter(function(x) {
					return x.type === "domain";
				});
				domains.sort(function(x, y) {
					return x.properties.index > y.properties.index;
				});
				playerObj = layer.objects.filter(function(x) {
					return x.type === "player";
				});
				buttons = layer.objects.filter(function(x) {
					return x.type === "button";
				});
				buttons.sort(function(x, y) {
					return x.properties.index > y.properties.index;
				});
				doors = layer.objects.filter(function(x) {
					return x.type === "door";
				});
				doors.sort(function(x, y) {
					return x.properties.index > y.properties.index;
				});
				temple = layer.objects.filter(function(x) {
					return x.type === "temple";
				});
				tower = layer.objects.filter(function(x) {
					return x.type === "tower";
				});
				shepherdObj = layer.objects.filter(function(x) {
					return x.type === "shepherd";
				});
				flySign = layer.objects.filter(function(x) {
					return x.type === "flysign";
				});

				// All objects in their own containers

				player = Crafty.e('Snake')._Snake(playerObj[0].x, playerObj[0].y, [2, 3, 3, 0, 3, 2, 2, 2, 1, 2, 1]);
				shepherd = Crafty.e('2D,Canvas, Shepherd').attr({
					x: shepherdObj[0].x,
					y: shepherdObj[0].y,
					z: zLevels['snake'] - 40
				});
				Crafty.e('2D,Canvas,Image, _Temple').image(imageMap['temple']).attr({
					x: temple[0].x,
					y: temple[0].y,
					z: zLevels['background'] + 10
				});
				Crafty.e('2D,Canvas,Image').image(imageMap['tower']).attr({
					x: tower[0].x,
					y: tower[0].y,
					z: zLevels['background'] + 10
				});
				flyText = Crafty.e('2D, Canvas, Collision').attr({x: flySign[0].x, y: flySign[0].y, w: flySign[0].width, h: flySign[0].height});
				
        flyText.onHit('SnakeBlock',
          function(){
            info.setText(["Congratulations, you made it to the top!", "As a reward, hold X to use your wings!", "Is there more to life, you ask?", "We're afraid not.", 'Good day.']);
            var dragon = this.hit('Snake');
            if (dragon) {
              info.setText(["Congratulations, you made it to the top!", "As a reward, hold X to use your wings!", "Is there more to life, you ask?", "We're afraid not.", 'Good day.'])
              dragon[0].obj.hasWings = true;
            }
           
          },
          function(){});
        // flyText.bind("EnterFrame", function() {
					// var dragon = this.hit('Snake');
					// if (dragon) {
						// info.setText(["Congratulations, you made it to the top!", "As a reward, hold X to use your wings!", "Is there more to life, you ask?", "We're afraid not.", 'Good day.'])
						// dragon[0].obj.hasWings = true;
					// }
				// });


				// invitation = Crafty.e('2D, AngularMotion, Delay, Tween, Canvas,  Moving, Collision, invitation').attr({x: player.x, vy:0.5, vx:0.1, y:player.y - 300});
				// //invitation.requires("WiredHitBox");
				// invitation.origin(24,-25);
				// invitation.rotation = -45;
				// invitation.rotateFactor =2;
				// //invitation.arotation = 100;

				// invitation.bind('EnterFrame', function(){          
				//   this.arotation = -this.rotateFactor*this.rotation;
				// })
				// invitation.moveCollisionTest = function(){
				//   if (this.hit("Platform")){
				//     this.rotateFactor = this.rotateFactor/1.1;
				//     this.tween({rotation:0},500);
				//     this.delay(function(){
				//       this.removeComponent("Moving");
				//       this.unbind("EnterFrame");
				//       this.bind("EnterFrame", function() {
				//       	if (this.hit("SnakeBlock")) {
				//       		info.setText(info.texts["invitation"]);
				//       	}
				//       });
				//     },1000,0);
				//     return true;
				//   }            
				//   return false;
				// };
				// invitation._Moving();

				for (var j = 0; j < sheep.length; j++) {
					var obj = sheep[j];
					for (var k = 0; k < obj.properties.number; k++) {
						var x = obj.x + Math.random() * obj.width;
						var y = obj.y + Math.random() * obj.height;
						var domain = domains[obj.properties.index];
						var xMin = domain.x;
						var xMax = domain.x + domain.width;
						var yMin = domain.y;
						var yMax = domain.y + domain.height;
						var shep = Crafty.e('Sheep')._Sheep(x, y, xMin, xMax);
						if (j === 0) {
							var tDomain = domains[1];
							var tXMin = tDomain.x;
							var tXMax = tDomain.x + tDomain.width;
							var tYMin = tDomain.y;
							var tYMax = tDomain.y + tDomain.height
							shep.requires("DomainCondition")
								._DomainCondition(tXMin, tXMax, tYMin, tYMax, function() {
									this.setDomainBounds(tXMin, tXMax, tYMin, tYMax);
									goals.sheepReturned = true;
								}, -1);
							// shep.bind("EnterFrame", function() {
							// 	if(this.fulfilled()) {
							// 		this.setDomainBounds(tXMin, tXMax, tYMin, tYMax);
							// 	}
							// });
						} else if (j === 1) {
							tDomain = domains[2];
							var tXMin = tDomain.x;
							var tXMax = tDomain.x + tDomain.width;
							var tYMin = tDomain.y;
							var tYMax = tDomain.y + tDomain.height;
							shep.requires("DomainCondition")
								._DomainCondition(tXMin, tXMax, tYMin, tYMax, function() {
									this.setDomainBounds(tXMin, tXMax, tYMin, tYMax);
									this.unbind("EnterFrame", this.enterFrameProgressCheck);
									this.removeComponent("XProgressCondition");
									if (!goals.sheepHerded) {
										goals.sheepHerded = true;
                    Crafty.trigger('ReachedGoal'); 
                    
									}
                  //Crafty.trigger('ReachedGoal'); 
                  
								}, obj.properties.group);
							shep.requires("XProgressCondition")._XProgressCondition(1, 500,
								function() {
									return function() {
										this.setDomainBounds(xMin, xMax, yMin, yMax);
									}
								}().bind(shep),
								function() {
									return function() {
										this.setDomainBounds(tXMin, tXMax, tYMin, tYMax);
                    Crafty.trigger('ReachedGoal'); 
                    
									};
								}().bind(shep)
							);
						}
					};
				}

				for (var j = 0; j < buttons.length; j++) {
					var obj = buttons[j];
					var x = obj.x;
					var y = obj.y;
					Crafty.e('Button')
						._Button(obj.properties.group)
						.attr({
							x: obj.x,
							y: obj.y,
							w: 48,
							h: 48
						});
				}

				// handle doors
				var btnsObj = Crafty("Button, Sheep");
				//var sheepObj = Crafty("Sheep");
				var uniqueDoors = doors[doors.length - 1].properties.index + 1;
				var doorLists = new Array(uniqueDoors);
				for (var j = doorLists.length - 1; j >= 0; j--) {
					doorLists[j] = [];
				};
				var matchingBtns = new Array(uniqueDoors);
				// create list of matching buttons
				for (var j = matchingBtns.length - 1; j >= 0; j--) {
					matchingBtns[j] = [];
				};
				for (var j = 0; j < doors.length; j++) {
					var obj = doors[j];
					var x = obj.x;
					var y = obj.y;
					var idx = obj.properties.index;
					doorLists[idx].push(obj);
					if (!matchingBtns[idx].length) {
						// this is the first doorblock to be processed,
						// populate the matching buttons list to be used
						// by the remaining door blocks of the same index.
						for (var k = btnsObj.length - 1; k >= 0; k--) {
							var btn = btnsObj[k];
							var realButton = Crafty(btn);
							if (realButton.group === idx) {
								matchingBtns[idx].push(realButton);
							}
						};
					}
				}
				for (var j = doorLists.length - 1; j >= 0; j--) {
					var door = Crafty.e('Door');
          door.group = j; // added by Anneroos

					if (j === 1) {
						door._Door(doorLists[j], matchingBtns[j], function() {
							info.setText(["Look at them eat!", "That's quite a big hole in the mountain..."]);
						});
					} else {
						door._Door(doorLists[j], matchingBtns[j]);
					}


				};
			}
		}
		for (var row = 0; row < level.height; ++row) {
			for (var col = 0; col < level.width; ++col) {
				// create tile for platform layer
				tileIdx = solidLayer.data[level.width * row + col];
				if (tileIdx != 0) {
					Crafty.e('2D, Canvas, Platform')._Platform(col, row).attr({
						z: zLevels['wall'],
						tileIdx: tileIdx
					}).sprite((tileIdx - 1) % xTiles, Math.floor((tileIdx - 1) / xTiles));
				}
				tileIdx = bgLayer.data[level.width * row + col];
				if (tileIdx != 0) {
					Crafty.e('2D, Canvas, Background')._Background(col, row).sprite((tileIdx - 1) % xTiles, Math.floor((tileIdx - 1) / xTiles));
				}
				tileIdx = tunnelLayer.data[level.width * row + col];
				if (tileIdx != 0) {
					Crafty.e('2D, Canvas, Background, Tunnel')._Background(col, row).sprite((tileIdx - 1) % xTiles, Math.floor((tileIdx - 1) / xTiles));
				}
			};
		};
		//Crafty.e("FireballSpawner")._FireballSpawner(20, true, 0, 500, false, (5 / 4) * Math.PI, (7 / 4) * Math.PI);
	}
	buildLevel();
});
