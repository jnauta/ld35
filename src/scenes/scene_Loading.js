// LOADING SCENE
Crafty.scene('Loading', function() {
	// // Draw some text for the player to see in case the file
	// //  takes a noticeable amount of time to load

	//Crafty.background('url(assets/images/background1.png)');
    Crafty.background(mycolors.background);
	
	Crafty.e('2D, DOM, Text')
		.attr({
			x: 200,
			y: 200,
			w: 400,
			h: 200,
			z: 8
		})
		.textFont({
			size: '30px',
			family: fontFamily1
		})
		.css({
			'padding-top': '45px',
			
			'border': ('2px dashed' + mycolors.button1),
			'border-radius': '8px',
			'cursor': 'pointer',
			'text-align': 'center',
			'padding-top': '3px',
			'color':'black',
		})
		.text('<BR><br>Loading,<br><BR> please wait...');
	Crafty.load(assetsObject,
		function() {
			//when loaded
			console.log('everything loaded!');
			
			// Crafty.sprite(24, 24, "assets/images/tiles.png", {
				// solidTile: [0,0],
			// });
      
      Crafty.sprite(48, 48, "assets/images/wallsprite.png", {
				solidTileNew: [0,0],
        grass: [1, 7],
        topGrass: [1, 6],
        rock1: [1, 13],
        rock2: [2, 13],
        rock3: [3, 13],
        rock4: [4, 13],
        mud: [0, 18],
        overgrowth: [1, 18],
			});
      
      doorSpriteLists = {
      	rock: ["rock1", "rock2", "rock3", "rock4"],
      	overgrowth: ["overgrowth"],
      	door: ["door"],
      };

      Crafty.sprite(24, 24, "assets/images/sprites.png", {
				button: [6,0,2,2],
        fireball: [10,0],
        invitation: [2,0,2,2],
        sheep: [10,1],
        shepherd: [4,0,2,2],
        grassTop: [2,6],
        grassBottom: [2,6],
        door: [0,0,2,2],
        banner: [0,2,16,3],
        music: [12,0,2,2],
        
        
			});
      
      Crafty.sprite(32, 24, "assets/images/snakesprite.png", {
				snakepart: [0,0],
        
			});
			
			Crafty.scene('Main');
		},

		function(e) { // onProgress
			//console.log(e);
		},

		function(e) {
			console.log('loading error');
			console.log(e);
		}
	);
    
    
    

});