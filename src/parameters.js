zLevels = {
	background: 1000,
	wall: 2000,
  snake: 3000,
  door: 4000,
  
};

tileSize = 0;

gravityForce = 0.4;
gravityMaxSpeed = 6;
jumpSpeed = 6;
jumpSustain = 0.5; //(should be between 0 and 1);
jumpFrames = 10;
wingFactor = 0.15;

minSnakeSize = 4;
maxSnakeSize = 9;
snakeFirstFrames = 8;
snakeFrames = 8;

playerAcceleration = 0.3;
playerSlowDown = 0.9; // factor between 0 and 1

fireballSpeed = 2.0;

sheepAcceleration = 0.2;
sheepSlowdown = 0.9;

textDuration = 300;

goals = {
  sheepReturned: false,
  sheepHerded: false,
};

levelIdx = 0;