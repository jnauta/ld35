Crafty.c('Platform',{
	init: function(){
		this.requires('2D, Canvas, OriginCoordinates, Collision');
		//this.color("mycolors.platformcolor");
		this.h = this.h ? this.h : 48;
		this.w = this.w ? this.w : 48;
		this.z = zLevels['platforms'];
		this.origin('center');
    //this.requires("WiredHitBox");
		this.sticky = false;
	},
	
	_Platform: function(x,y){
		this.requires('solidTileNew').sprite(0,0);
		this.x = 48*x;
		this.y = 48*y;
		return this;
		
	},
	
}); 