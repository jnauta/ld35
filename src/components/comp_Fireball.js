Crafty.c("Fireball", {
  init: function() {
    this.requires("2D, Canvas, fireball, Collision").attr({w: 24, h: 24});
  },
  _Fireball: function(xpos, ypos, vx, vy) {
    this.x = xpos;
    this.y = ypos;
    this.vx = vx;
    this.vy = vy;
    this.bind("EnterFrame", function(){
      this.x += this.vx;
      this.y += this.vy;
      if (this.hit ("Platform")) {
        this.destroy();
      }
      var snake = this.hit("SnakeBlock");
      if (snake) {
        snake[0].obj._parent.damage();
        this.destroy();
        var random = Math.floor(Math.random()*4)+1
        Crafty.audio.play('grom'+ random.toString());
      }
    })
  }
});

Crafty.c("FireballSpawner", {
  init: function(){},
  // spawns fireballs at y = yLine and x between minX and maxX.
  // x and y may be relative to the viewport or absolute.
  // angles in radians determine the direction in which fireballs fire.
  _FireballSpawner: function(yLine, yViewportRelative, minX, maxX, xViewportRelative, minAngle, maxAngle){
    this.bind("EnterFrame", function() {
      if (Math.random() > 0.97) {
        direction = minAngle + Math.random() * (maxAngle - minAngle);
        var vx = Math.cos(direction) * fireballSpeed;
        var vy = -Math.sin(direction) * fireballSpeed;
        var x = minX + Math.random() * (maxX - minX);
        if (xViewportRelative) {
          x -= Crafty.viewport._x;
        }
        var y = yLine - Crafty.viewport._y;
        Crafty.e("Fireball")._Fireball(x, y, vx, vy);
      }
    });
  }
});