// NOT IN USE

Crafty.c("CameraFocus", {

  init: function() {
    this.requires("2D, Canvas, Moving").attr({w: 5, h: 5})._Moving();
  },

  _CameraFocus: function(obj) {
    this.x = obj._x;
    this.y = obj._y;
    this.speed = 0;
    this.acceleration = 0;

    this.referenceVector = new Crafty.math.Vector2D(1, 0);
    this.myVec = new Crafty.math.Vector2D();
    this.targetVec = new Crafty.math.Vector2D();
    this.bind("EnterFrame", function(){
        this.myVec.setValues(this._x, this._y);
        this.targetVec.setValues(obj._x, obj._y);
        var diffVec = this.targetVec.subtract(this.myVec);
        var d = diffVec.magnitude();
        if (d > 10) {
          this.acceleration += (d-10) * 0.001;
          this.speed += this.acceleration;
        }
        var angle = this.referenceVector.angleTo(diffVec);
        this.vx = Math.cos(angle) * this.speed;
        this.vy = Math.sin(angle) * this.speed;
        if (!obj.snakeMode && d < 1) {
          this.x = obj.x;
          this.y = obj.y;
        }
        this.acceleration *= 0.92;
        this.speed *= 0.96;
        if (this.acceleration < 0.001) {
          this.acceleration = 0;
        }
        if (this.speed < 0.01) {
          this.speed = 0;
        }
    });
    //Crafty.viewport.follow(this);//, -Game.width() / 4, -Game.height() / 4);
    return this;
  },

  moveCollisionTest: function() {return false;}

});