// Crafty.c("ViewportRelative", {
  // _viewportPreviousX: 0,
  // _viewportPreviousY: 0,
  // _viewportStartX: 0,
  // _viewportStartY: 0,
  // init: function() {
    // this.bind("EnterFrame", this._frame);
  // },
  // _frame: function() {
    // if (this._viewportPreviousX != Crafty.viewport._x) {
      // this._viewportStartX = Crafty.viewport._x;

      // this.x += this._viewportPreviousX
      // this.x -= Crafty.viewport._x;

      // this._viewportPreviousX = this._viewportStartX;
    // }

    // if (this._viewportPreviousY != Crafty.viewport._y) {
      // this._viewportStartY = Crafty.viewport._y;

      // this.x += this._viewportPreviousY
      // this.x -= Crafty.viewport._y;

      // this._viewportPreviousX = this._viewportStartX;
    // }
  // }
// });

Crafty.c("Info", {
  init: function() {
    this.requires("2D, DOM, Text").attr({
      x: 0,
      y: 0,
      h: 42,
      w: 354,
      z: 4
    });
    this.bannerImage = Crafty.e("2D,DOM,banner").attr({
      z: 0,
      x: 0,
      y: 0,
      h: 72,
      w: 384
    });
    // this.bind(this.bannerImage);
    this.visible = false;
    this.bannerImage.visible = false;
    this.alpha = 0.7;
    this.textCountdown = 0;
    this.textIdx = 0;
    this.lines = [];

    this.textFont({
      size: '20px',
      family: fontFamily1
    });
    this.text(this.texts.start);
    this.css({
      'text-align': 'center',
      'color': '#777777',
      'border': '1px solid ' + mycolors.infoborder,
      'border-radius': '1px',
      'cursor': 'default',
      'padding': '15px'
    });
    
    this.musicButton = Crafty.e('2D, DOM, Mouse, music');
    this.musicButton.attr({x:5, y:155, w:24, h:24, z:6009}).css({'cursor':'pointer'}).bind('Click',function(){
      //Crafty.audio.togglePause('bgmusic')
      if(mutemusic){
        
        mutemusic = false;
        // Crafty.audio.togglePause('bgmusic');   
        if(bgMusic){
          Crafty.audio.unpause("bgmusic");
          //bgMusic.unpause();	
        }

        this.sprite(12,0);
        // Crafty.DrawManager.renderDOM();
      } 
      else {
        mutemusic = true;
        
        // Crafty.audio.togglePause('bgmusic'); 
        if (bgMusic) {
          Crafty.audio.pause("bgmusic");
          // bgMusic.pause();
        }
        this.sprite(14,0);
        // Crafty.DrawManager.renderDOM();
      }
    });
  
    if(mutemusic){this.musicButton.sprite(14,0,2,2);}
  

    
    
    this.bind('EnterFrame', function() {
      // Make relative to viewport
      this.x = -Crafty.viewport._x + 208;
      this.y = -Crafty.viewport._y + 20;
      this.bannerImage.x = this.x;
      this.bannerImage.y = this.y;
      this.musicButton.x = this.x + 567;
      this.musicButton.y = this.y + -15;
      
      if (this.textCountdown === 0) {
        if (this.textIdx < this.lines.length) {
          this.nextText();
        } else if (this.textIdx === this.lines.length) {
          this.lines = [];
          this.textIdx = 0;
          this.visible = false;
          this.bannerImage.visible = false;
        }
      } else {
        --this.textCountdown;
      }
      
    });
  },

  texts: {
    start: ['Hold <span style ="color:red"> z </span> to go into snake mode.'],
    invitation: ['Dear Dragon, would you like to join my party tonight on top of the tower?'],
    shepherdAsks: ['Honorable dragon!', 'My shnoogles are hungry and cannot reach the meadow.', 'Will you help them?', 'Please hold \'Z\' and follow me.'],
    shepherdAskHerd: ['The meadow is up on that plateau'],
    shepherdThanks: ['Thank you, Dragon.', 'Wings be with you.'],
    shepherdAskReturn: ["Oh no! Where is Ihu?!", "He must have gotten lost in the hills in the west...", "Please return him to me!"]
  },

  setText: function(theTexts) {
    if (!this.lines.length) {
      this.lines = theTexts; // array of text fragments
      this.textCountdown = 0; // added by Anneroos // textDuration;
    }
  },
  nextText: function() {
    this.text(this.lines[this.textIdx]);
    this.visible = true;
    this.bannerImage.visible = true;
    ++this.textIdx;
    this.textCountdown = textDuration;
  }
});