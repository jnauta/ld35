Crafty.c("MovingActor", {
  init: function() {},

  _MovingActor: function(acc, xSlowFactor) {
    this.acc = acc;
    this.xSlowFactor = xSlowFactor;
    return this;
  },

  updateVelocity: function() {
    if (this.goingRight) {
      this.vx += this.acc;
    }
    if (this.goingLeft) {
      this.vx -= this.acc;
    }
    this.vx *= this.xSlowFactor;
    if (this.vx > -0.001 && this.vx < 0.001) {
      this.vx = 0;
    }


    var maxFallV = gravityMaxSpeed;
    if (this.wingsOut) {
      maxFallV *= wingFactor;
    }
    if (this.has("GForce") && this.vy < maxFallV) {
      if (!this.jumping) {
        if (this.wingsOut) {
          this.vy += wingFactor * gravityForce;
        } else {
          this.vy += gravityForce;
        }
      } else {
        if (this.wingsOut) {
          this.vy += wingFactor * (1 - jumpSustain) * gravityForce;
        } else {
          this.vy += (1 - jumpSustain) * gravityForce;
        }
      }
    }
    if (this.jumping) {
      --this.jumpCounter;
      if (this.jumpCounter === 0) {
        console.log("jump ends");
        this.jumping = false;
        this.jumpCounter = jumpFrames;
      }
    }
  }
});