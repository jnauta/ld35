Crafty.c("Moving", {
	init: function(){
		this.requires("2D");
		this.vx = 0.0;
		this.vy = 0.0;
		this.realVX = 0.0;
		this.realVY = 0.0;
		this.speed = 0.2;
	},

	_Moving: function() {
		this.bind("EnterFrame", function() {
			this.prevX = this.x;
			this.x = this.x + this.vx;//Math.round(this.x + this.vx);
			if (this.moveCollisionTest("x")) {
				this.x = this.prevX;
				this.vx = 0.0;
			}
			this.prevY = this.y;
			this.y = this.y + this.vy; //Math.round(this.y + this.vy);
			if (this.moveCollisionTest("y")) {
				this.y = this.prevY;
				//necessary to get close to ground
				// => exact fitting of sprites.
				this.vy *= 0.01;
				if (this.vy > 0) {
					this.airborne = false;
					this.jumping = false;
          if(this.has("Sheep")){
            this.sprite(10,1);
          }
					this.jumpCounter = 0;
				}
			}
			this.realVX = this.x - this.prevX;
			this.realVY = this.y - this.prevY;
		});
		return this;
	}
});