Crafty.c('Background', {
	init: function(){
		this.requires('2D, Canvas, solidTileNew, OriginCoordinates');
		//this.color("mycolors.platformcolor");
		this.h = 48;
		this.w = 48;
		this.z = zLevels['background'];
		this.origin('center');
	},
	
	_Background: function(x,y){
		this.x = 48*x;
		this.y = 48*y;
		return this;
	},
});