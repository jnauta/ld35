Crafty.c("Sheep", {
  init: function() {
    this.requires("2D, Canvas,  Collision, Moving, sheep, MovingActor, GForce")
      ._Moving()
      ._MovingActor(sheepAcceleration, sheepSlowdown)
      .attr({
        w: 23,
        h: 23,
        z: zLevels['snake'] + 30
      });
    //var random = Math.floor(Math.random()*2)+1;
    //this.image(imageMap['sheep' + random.toString()])
  },
  _Sheep: function(x, y, xLeft, xRight) {
    this.x = x;
    this.y = y;
    this.xLeft = xLeft;
    this.xRight = xRight;
    this.waitCountdown = 100;
    this.walkCountdown = 0;
    this.wallBounce = false;
    this.bind("EnterFrame", function() {
      

        
      if (this.waitCountdown === 0) {
        if (this.x < xLeft) {
          this.dir = 1;
        } else if (this.x > xRight) {
          this.dir = -1;
        } else if (Math.random() > 0.5) {
          this.dir = 1;
        } else {
          this.dir = -1;
        }
        this.wallBounce = false;
        this.waitCountdown = -1;
        this.walkCountdown = 100 + Math.round(Math.random() * 40);
      } else if (this.x + Crafty.viewport._x >= -10 &&  this.x + Crafty.viewport._x <= 810 ){
        --this.waitCountdown;
      }

      if (this.walkCountdown === 0) {
        this.dir = 0;
        this.walkCountdown = -1;
        this.waitCountdown = 100;
      } else {
        --this.walkCountdown;
      }

      if (((this._x < this.xLeft && this.goingLeft) || (this._x > this.xRight && this.goingRight)) && !this.wallBounce) {
        this.dir *= -1;
      }

      if (this.dir !== 0) {
        if (!this.airborne) {
          this.vy -= jumpSpeed;
          this.jumping = true;
          this.airborne = true;
          this.sprite(11, 1);
        }
      }

      if (this.dir === 1) {
        this.unflip("X")
        this.goingRight = true;
        this.goingLeft = false;
      } else if (this.dir === -1) {
        this.flip("X");
        this.goingLeft = true;
        this.goingRight = false;
      } else {
        this.goingLeft = false;
        this.goingRight = false;
      }
      this.updateVelocity();
      
    });
    return this;
  },
  setDomainBounds: function(xMin, xMax) {
    this.xLeft = xMin;
    this.xRight = xMax;
  },
  moveCollisionTest: function(dir) {
    if (this.hit("Platform")) {
      if (dir === "x") {
        this.dir *= -1;
        this.wallBounce = true;
      }
      return true;
    }
    if (this.hit("Tunnel")) {
      return true;
    }
    if (this.hit("SnakeBlock")) {
      return true;
    }
  }

});

Crafty.c("Shepherd", {
  init: function() {
    this.requires("2D, Canvas, Moving, MovingActor, Collision, shepherd, GForce, Delay")._Moving()._MovingActor(sheepAcceleration, sheepSlowdown);
    this.goingLeft = true;
    this.bind("EnterFrame", this.updateVelocity);
    this.explainFun = function() {
      if (Math.abs(player._x - this._x) < 100 && Math.abs(player._y - this._y) < 100) {
        if (!goals.sheepReturned && !goals.sheepHerded) {
          info.setText(info.texts["shepherdAskHerd"]);
        } else if (!goals.sheepReturned && goals.sheepHerded) {
          info.setText(info.texts["shepherdAskReturn"]);
        } else if (goals.sheepHerded && goals.sheepReturned) {
          if (maxSnakeSize !== 12) {

            var dragon = Crafty('Snake');
            dragon = Crafty(dragon[0]);
            dragon.blocks[maxSnakeSize - 1].bodyImage.sprite(2, 0);
            for (var extraBlock = maxSnakeSize; extraBlock < 12; extraBlock++) {
              dragon.blocks[extraBlock] = Crafty.e("SnakeBlock").attr({
                w: dragon.blockSize,
                h: dragon.blockSize,
                z: zLevels['snake'] - extraBlock
              });
              dragon.attach(dragon.blocks[extraBlock]);
              dragon.blocks[extraBlock].bodyImage.attr({
                z: zLevels['snake'] - extraBlock
              });
            };
            var oldSize = dragon.snakeSize;
            dragon.snakeSize += 12 - maxSnakeSize;
            maxSnakeSize = 12;
          }
          info.setText(info.texts["shepherdThanks"]);
        } else {
          info.setText(info.texts["shepherdAskHerd"]);
        }
      }
    };
    this.delay(
      function() {
        // console.log('jump!');
        this.jump();
      }, 600, 2, // jumping twice after 600 ms
      function() { // and then I plan on...
        this.delay(function() { // stop going left
          this.goingLeft = false;
          info.setText(info.texts["shepherdAsks"]);
          this.delay(function() {
            // console.log('now going right');
            this.goingRight = true;
            this.flip("X");
            this.delay(function() {
              // console.log('planning on planning to jump');
              this.delay(function() {
                // console.log('planning to jump again...');
                this.delay(function() {
                    // console.log('second jump');
                    this.jump();
                  },
                  600, 2); // jump interval near sheep
              }, 4000, 0, function() {
                // console.log('planning to stop going right');
                this.delay(function() {
                  // console.log('stop going right now');
                  this.goingRight = false;
                  this.bind("EnterFrame", function() {
                    this.explainFun();
                  });
                }, 3000);
              }); // end of second function.
            }, 1000); // time before shepherd says where sheep should go
          }, 10000); // time that shepherd talks to us.
        }, 1000); // stop going left after 1 second
      });
  },
  _Shepherd: function(sheep) {
    this.sheep = sheep;
  },
  jump: function() {
    if (!this.airborne) {
      this.vy -= jumpSpeed;
      this.jumping = true;
      this.airborne = true;
    }
  },
  moveCollisionTest: function(dir) {
    if (this.hit("Platform")) {
      if (dir === "x") {
        this.dir *= -1;
      }
      return true;
    }
    if (this.hit("Tunnel")) {
      return true;
    }
  }
})