Crafty.c("Door", {
  init: function() {

  },
  
  // conditions: an array of objects with a fulfilled function
  // succFun, when all conditions are fulfilled, do this?
  _Door: function(doorBlocks, conditions, succFun) {
    this.doorBlocks = new Array();
    for (var i = doorBlocks.length - 1; i >= 0; i--) {
      var doorBlockObj = doorBlocks[i];
      var sprites = doorSpriteLists[doorBlockObj.properties.sprite];
      var sprite = sprites[Math.floor(Math.random() * sprites.length)];
      this.doorBlocks.push(Crafty.e("DoorBlock")._DoorBlock(sprite).attr({
        x: doorBlocks[i].x,
        y: doorBlocks[i].y
      }));
    };
    
    for (var i = conditions.length - 1; i >= 0; i--) {
      conditions[i].door = this;
    };
    
    
    this.checkConditions = function(){
      var fulfilled = 0;
      for (var j = 0; j < conditions.length; j++) {
        if (!conditions[j].fulfilled()) {
          break;
        };
        ++fulfilled;
      };
      if (fulfilled === conditions.length) {
        if (succFun) {
          succFun();
        }
        for (var k = this.doorBlocks.length - 1; k >= 0; k--) {
          this.doorBlocks[k].destroy();
        };
        this.destroy();
      }
    };
    
    this.bind("ReachedGoal", function() {
      this.checkConditions();
    });
    return this;
  }
});

Crafty.c("DoorBlock", {
  init: function() {
    this.requires("2D, Canvas, Platform");
  },
  _DoorBlock: function(sprite) {
    this.addComponent(sprite);
    this.w = 48;
    this.h = 48;
    this.z = zLevels['door'] - 10;
    return this;
  }
});

Crafty.c("Button", {
  init: function() {
    this.requires("2D, AngularMotion, Canvas, Collision, button").attr({
      w: 48,
      h: 48,
      z: zLevels["background"] + 10,
    }).origin("center");
    this.collision([4, 24, 8, 8, 24, 4, 40, 8, 44, 24, 40, 40, 24, 44, 8, 40]);
    this.touched = false;
    this.touch = function(){
      // Fire an event to check if all buttons for this door are fulfilled
      this.door.checkConditions();
      this.touched = true;
      this.sprite(8, 0, 2, 2);
      this.vrotation = 360;
    };
    this.untouch = function(){
      this.touched=false;
      this.sprite(6, 0, 2, 2);
      this.vrotation = 0;
    };
    this.onHit("SnakeBlock",this.touch,this.untouch);
    // this.bind("EnterFrame", function() {
      // if (this.hit("SnakeBlock")) {
        // this.touch();
        
      // } else {
        // this.untouch();
      // }
    // });
  },
  _Button: function(btnGroup) {
    this.group = btnGroup;
    // this .door?
    return this;
  },
  fulfilled: function() {
    return this.touched;
  }
});

Crafty.c("DomainCondition", {
  init: function() {},
  _DomainCondition: function(minX, maxX, minY, maxY, succFun, conditionGroup) {
    this.minX = minX;
    this.maxX = maxX;
    this.group = conditionGroup;
    this.bind("EnterFrame", function() {
      if (this.fulfilled()) {
        succFun.call(this);
      }
    });
    return this;
  },
  fulfilled: function() {
    return (this.x > this.minX && this.x < this.maxX);
  }
});

Crafty.c("XProgressCondition", {
  init: function() {},
  // direction -1 = left, 1 = right
  _XProgressCondition: function(direction, frames, failFun, succFun) {
    this.direction = direction;
    this.frames = frames;
    this.failFun = failFun;
    this.succFun = succFun;
    this.targetFrame = Crafty.frame() + frames;
    this.oldX = this._x;
    this.bind("EnterFrame", this.enterFrameProgressCheck);
    return this;
  },
  enterFrameProgressCheck: function() {
    if (Crafty.frame() === this.targetFrame) {
      var deltaX = this._x - this.oldX;
      if (Math.sign(deltaX) === this.direction && Math.abs(deltaX) > 5) {
        this.succFun();
      } else {
        this.failFun();
      }
      this.targetFrame = Crafty.frame() + this.frames;
      this.oldX = this._x;
    }
  }

});