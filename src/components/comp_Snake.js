Crafty.c("Snake", {
  blockSize: 24,
  hasWings: false,
  wingsOut: false,

  init: function() {
    this.requires('2D, KeyControls, Moving, MovingActor, GForce')._KeyControls(Crafty.keys.LEFT_ARROW, Crafty.keys.RIGHT_ARROW, Crafty.keys.UP_ARROW, Crafty.keys.DOWN_ARROW, Crafty.keys.Z, Crafty.keys.X)
      ._Moving()
      ._MovingActor(playerAcceleration, playerSlowDown);
    
    this.snakeSize = maxSnakeSize;
    this.actualSize = this.snakeSize;
    this.blocks = new Array(maxSnakeSize);
    for (var i = 0; i < maxSnakeSize; i++) {
      this.blocks[i] = Crafty.e("SnakeBlock").attr({
        w: this.blockSize,
        h: this.blockSize,
        z: zLevels['snake'] - i
      });
      this.attach(this.blocks[i]);
      this.blocks[i].bodyImage.attr({
        z: zLevels['snake'] - i
      });
    };

    // body and tail different image
    this.blocks[0].bodyImage.sprite(1, 0);
    this.blocks[maxSnakeSize - 1].bodyImage.sprite(2, 0);

    // for (var i = 0; i < this.snakeSize; i++) {
    //   if (i !== this.snakeSize - 1) {
    //     this.blocks[i].setNxt(this.blocks[i+1]);
    //   };
    //   if (i !== 0) {
    //     this.blocks[i].setPrv(this.blocks[i-1]);
    //   }
    // };
    this.connections = new Array(maxSnakeSize - 1);
    // this.setShape([2, 3, 3, 0, 3, 2, 2, 2, 1, 2, 1]); // nth elements indicates how n+1 relates to n.
    // this.layoutBody();

    //Crafty.e("CameraFocus")._CameraFocus(this);
    Crafty.viewport.follow(this);
    Crafty.viewport.bounds = null; //{min:{x:0, y:0}, max:{x:500, y:500}};
  },

  _Snake: function(resetX, resetY, resetShape) {
    this.x = resetX;
    this.resetX = resetX;
    this.y = resetY;
    this.resetY = resetY;
    this.resetShape = resetShape;
    this.setShape(this.resetShape);//[2, 3, 3, 0, 3, 2, 2, 2, 1, 2, 1]); // nth elements indicates how n+1 relates to n.
    this.layoutBody();
    return this;
  },
  /*
  newShape is an array of size snakeSize - 1
  numbers 0, 1, 2, 3 indicate connections from head to tail
  0 = left
  1 = down
  2 = right
  3 = up
  */
  setShape: function(newShape) {
    for (var i = 0; i < this.actualSize - 1; i++) {
      this.connections[i] = newShape[i];
    }
  },

  addDirToObjPosition: function(obj, dir) {
    if (dir === 0) { // left
      obj.x -= this.blockSize;
    } else if (dir === 1) { // down
      obj.y += this.blockSize;
    } else if (dir === 2) { // right
      obj.x += this.blockSize;
    } else if (dir === 3) { // up
      obj.y -= this.blockSize;
    }
  },

  layoutBody: function() {
    var frontPos = {
      x: this.x,
      y: this.y
    };
    this.blocks[0].x = this.x;
    this.blocks[0].y = this.y;
    for (var i = 1; i < this.actualSize; i++) {
      var dir = this.connections[i - 1];
      this.addDirToObjPosition(frontPos, (dir + 2) % 4);
      this.blocks[i].setPosition(frontPos);
    }
    // for (var i = 0; i < this.blocks.length; i++) {
    //     console.log(this.blocks[i].x + " " + this.blocks[i].y);
    // };  

    // orientation head
    this.blocks[0].orient = this.connections[0];
    if (this.blocks[0].orient === 0) {
      this.blocks[0].bodyImage.flip("Y")
    } else if (this.blocks[0].orient === 2) {
      this.blocks[0].bodyImage.unflip("Y")
    }
    // orientation body parts
    for (var i = 1; i < this.actualSize - 1; i++) {
      if (this.connections[i] === this.connections[i - 1]) {
        this.blocks[i].bodyImage.sprite(0, 0);
        this.blocks[i].orient = this.connections[i];
      } else {

        if ((this.connections[i] - this.connections[i - 1] + 4) % 4 === 1) {
          this.blocks[i].bodyImage.sprite(3, 0);
          this.blocks[i].bodyImage.unflip("Y");
          this.blocks[i].orient = this.connections[i];
        } else {
          this.blocks[i].bodyImage.sprite(3, 0);
          this.blocks[i].bodyImage.flip("Y");
          this.blocks[i].orient = this.connections[i];
        }
      }
    }
    // orientation tail
    this.blocks[this.actualSize - 1].orient = this.connections[this.actualSize - 2]


    // rotate image accordingly
    for (var i = 0; i <= this.actualSize - 1; i++) {
      this.blocks[i].bodyImage.rotation = -90 * (this.blocks[i].orient + 2);
    }
  },

  damage: function() {
    --this.snakeSize;
    if (this.snakeSize < minSnakeSize) {
      this.reset();
    }
    if (this.snakeSize < this.actualSize) {
      // snake appearance must change
      this.actualSize = this.snakeSize;
      // tail different image
      this.blocks[this.actualSize - 1].bodyImage.sprite(2, 0);
      // make rest of snake invisible and stop colliding.
      for (var i = this.actualSize; i < maxSnakeSize; ++i) {
        var b = this.blocks[i];
        b.removeComponent("SnakeBlock");
        b.bodyImage.visible = false;
      };
      // connections gets shorter, grows again when increasing actualSize
      this.connections.pop();
      this.layoutBody();
    }
  },

  reset: function() {
    this.x = this.resetX;
    this.y = this.resetY;
    this.snakeSize = maxSnakeSize;
    this.actualSize = maxSnakeSize;
    this.setShape(this.resetShape);
    for (var i = 0; i < maxSnakeSize; i++) {
      this.blocks[i].addComponent("SnakeBlock").z = zLevels['snake'] - i;
      this.blocks[i].bodyImage.z = zLevels['snake'] - i;
    }
    this.blocks[this.actualSize - 1].bodyImage.sprite(2, 0);
    this.layoutBody();
  },

  heal: function() {
    if (this.snakeSize < maxSnakeSize) {
      ++this.snakeSize;
    }
  },

  snakeMove: function(dir) {
    var tryLonger = false;
    if (dir !== ((this.connections[0] + 2) % 4)) {
      // head is not going back
      this.addDirToObjPosition(this, dir);
      this.connections.unshift(dir);
      var lastTailMove = this.connections.pop();
      if (this.actualSize === this.snakeSize) {
        // if we have our currently allowed length, pop last connection
      } else {
        // we just got longer, make stuff correct
        tryLonger = true;
      };
      this.layoutBody();
    }
    if (this.moveCollisionTest()) {
      this.addDirToObjPosition(this, (dir + 2) % 4);
      this.connections.push(lastTailMove);
      this.connections.shift();
      this.layoutBody();
    } else{
        // if (!mutesound && !Crafty.audio.isPlaying("snakemodesound")){Crafty.audio.play("snakemodesound"); }
        if (tryLonger) {
        ++this.actualSize;
        this.connections.push(lastTailMove);
        var tailIndex = this.actualSize - 1;
        var b = this.blocks[tailIndex];
        b.addComponent("SnakeBlock").z = zLevels['snake'] - tailIndex;
        // tail different image
        b.bodyImage.sprite(2, 0);
        b.bodyImage.z = zLevels['snake'] - tailIndex;
        b.bodyImage.visible = true;
        this.layoutBody();
      }
    }
    Crafty.viewport.centerOn(this, 1 * (1000 / Crafty.timer.FPS()) * snakeFrames);
  },

  moveCollisionTest: function(dir) {
    for (var i = 0; i < this.actualSize; i++) {
      if (this.blocks[i].moveCollisionTest(dir)) {
        return true;
      }
    }
    for (var i = 0; i < this.actualSize; i++) {
      if (this.blocks[i].moveCollisionTestSheep(dir)) {
        return true;
      }
    }
    return false;
  }
});

Crafty.c("SnakeBlock", {
  init: function() {
    this.requires("2D, Canvas, Collision");
    //this.requires("WiredHitBox");
    this.prv = null;
    this.nxt = null;
    this.w = 24;
    this.h = 24;
    this.bodyImage = Crafty.e("2D, Canvas, snakepart");
    this.attach(this.bodyImage);
    this.bodyImage.attr({
      x: this.x - 4,
      y: this.y
    }).origin("center");
  },
  setPrv: function(prvBlock) {
    this.prv = prvBlock;
  },
  setNxt: function(nxtBlock) {
    this.nxt = nxtBlock;
  },
  setPosition: function(pos) {
    this.x = pos.x;
    this.y = pos.y;
  },
  moveCollisionTest: function(dir) {
    if (this.hit("_Temple")) {
      while (this._parent.snakeSize < maxSnakeSize) {
        this._parent.heal();
      }
    }
    if (this.hit('Platform')) {
      return true;
    }
    return false;
  },
  moveCollisionTestSheep: function(dir) {
    var sheepHits = this.hit("Sheep");
    if (sheepHits) {
      if (this._parent.snakeMode) {
        return true;
      } else {
        // push sheep and see if it/they collides with something
        for (var i = 0; i < sheepHits.length; i++) {
          var sheep = sheepHits[i].obj;
          var oldX = sheep.x;
          var oldY = sheep.y;
          if (dir === "x") {
            sheep.x = Math.round(sheep.x + this._parent.vx);
          } else if (dir === "y") {
            sheep.y = Math.round(sheep.y + this._parent.vy);
          }
          if (sheep.moveCollisionTest()) {
            if (dir === "x") {
              sheep.x = oldX;
            } else if (dir === "y") {
              sheep.y = oldY;
            }
            return true;
          }
        };
      }
    }
    return false;
  }
});