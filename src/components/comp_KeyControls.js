Crafty.c('KeyControls', {
	snakeMode: false,
	snakeDir: -1,
	snakeUp: false,
	snakeDown: false,
	snakeLeft: false,
	snakeRight: false,
	snakeCounter: snakeFrames,

	goingLeft: false,
	goingRight: false,
	jumping: false,
	jumpCounter: jumpFrames,
	airborne: true,

	init: function() {
		this.bind('KeyDown', function(keyEvent) {
			var k = keyEvent.key;
			if (k === this.up ) {
				if (this.snakeMode) {
					this.snakeUp = true;
					this.snakeDir = 3;
				} else {
					if (!this.airborne) {
						this.vy = -jumpSpeed;
						this.jumping = true;
						this.airborne = true;
					}
				}
			} else if (k === this.down) {
				if (this.snakeMode) {
					this.snakeDown = true;
					this.snakeDir = 1;
				} else {
					
				}
			} else if (k === this.left) {
				if (this.snakeMode) {
					this.snakeLeft = true;
					this.snakeDir = 0;
				} else {
					this.goingLeft = true;
				}
			} else if (k === this.right) {
				if (this.snakeMode) {
					this.snakeRight = true;
					this.snakeDir = 2;
				} else {
					this.goingRight = true;
				}
			} else if (k === this.snakeModeKey) {
				if (!this.wingsOut) {
					this.snakeMode = true;
					this.goingLeft = false;
					this.goingRight = false;
				}
			} else if (k === this.wingsKey && this.hasWings) {
				if (!this.snakeMode) {
					this.wingsOut = true;
				}
			}
		});
		this.bind('KeyUp', function(keyEvent) {
			var k = keyEvent.key;
			if (k === this.up ) {
				if (this.snakeMode) {
					this.snakeUp = false;
				} else {
					this.jumping = false;
				}
			} else if (k === this.down) {
				if (this.snakeMode) {
					this.snakeDown = false;
				} else {
					
				}
			} else if (k === this.left) {
				if (this.snakeMode) {
					this.snakeLeft = false;
				} else {
					this.goingLeft = false;
				}
			} else if (k === this.right) {
				if (this.snakeMode) {
					this.snakeRight = false;
				} else {
					this.goingRight = false;
				}
			} else if (k === this.snakeModeKey) {
				this.snakeMode = false;
				Crafty.viewport.follow(this);
				this.snakeRight = false;
				this.snakeLeft = false;
				this.snakeUp = false;
				this.snakeDown = false;
				this.snakeDir = -1;
			} else if (k === this.wingsKey) {
				this.wingsOut = false;
			}
		});
		this.bind('EnterFrame', function(){
			if (this.snakeUp || this.snakeDown || this.snakeLeft || this.snakeRight) {
				if (this.snakeCounter === 0) {
					this.snakeMove(this.snakeDir);
					this.snakeCounter = snakeFrames;
				}
			} else {
				this.snakeDir = -1;
			}
			if (this.snakeCounter !== 0) {
				--this.snakeCounter;
			}
			this.updateVelocity();
		});
	},
	_KeyControls: function(left, right, up, down, action1, action2) {
		this.left = left;
		this.right = right;
		this.up = up;
		this.down = down;
		this.snakeModeKey = action1;
		this.wingsKey = action2;
		return this;
	},

});