#!/usr/bin/python

import os

indexFile = open('index.html', 'w')


header = """<!DOCTYPE html>
<html>
  <head>
        <title>Wings for Ska Li</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
        <link rel="stylesheet" type="text/css" href="stylesheet.css" />
        <!-- font -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favoicon.png"> 
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans' rel='stylesheet' type='text/css'>
    
    <!--   <meta name="viewport"   content="width=device-width, user-scalable=no, initial-scale=1.0" /> -->
    <script src="bower_components/crafty/dist/crafty.js"></script>"""
customSources = """    <script src="assets/images/imageMap.js"></script>
    <script src="assets/sound/soundMap.js"></script>
    <script src="assets/assetsObject.js"></script>"""
footer = """    <script>
      window.addEventListener('load', Game.start);
    </script>
    
    <!--  Thumbnail voor Facebook enzo. ;) -->
    <link rel="image_src" href="assets/images/scrshot.png"/>  
  </head>
  <body>
    <div id="canvascontainer">  
      <div id="cr-stage"></div>
    </div>
    <div>
      
      <p>Made by <a href="http://jellenauta.com/games/">Jelle & Anneroos</a> with the 
        <a href="http://craftyjs.com">Crafty library</a>  for 
        <a href="http://www.ludumdare.com/compo/ludum-dare-35/?action=preview&uid=18490">Ludum Dare 35</a> with the theme 'Shapeshift'.
      </p> 
      <!-- <p> <a href="http://jellenauta.com/games/">Return to our game page</a></p> -->
    </div>
    
  </body>
</html>"""

try:
  indexFile.write(header)
  for root, dirs, files in os.walk('./src/'):
    for name in files:
      indexFile.write('    <script src="')
      indexFile.write(os.path.join(root, name))
      indexFile.write('"></script>\n')
  for root, dirs, files in os.walk('./assets/levels/'):
    files = [file for file in files if file.endswith( (".js") )]
    for name in files:
      indexFile.write('    <script src="')
      indexFile.write(os.path.join(root, name))
      indexFile.write('"></script>\n')
  indexFile.write(customSources)
  indexFile.write(footer)
finally:
  indexFile.close()