var imageMap = {
	bgsquare: 'assets/images/bgsquare.png',
	favoicon: 'assets/images/favoicon.png',
	scrshot: 'assets/images/scrshot.png',
	temple: 'assets/images/temple.png',
	sprites: 'assets/images/sprites.png',
	tower: 'assets/images/tower.png',
	snakesprite: 'assets/images/snakesprite.png',
	coins: 'assets/images/coins.png',
	wallsprite: 'assets/images/wallsprite.png',
};