var assetsObject = {
"audio": {
	"snakemodesound": ["snakemodesound.mp3"],
	"bgmusic": ["bgmusic.ogg", "bgmusic.mp3", "bgmusic.wav"],
	"slide": ["slide.mp3"],
	"grom4": ["grom4.mp3"],
	"grom1": ["grom1.mp3"],
	"grom2": ["grom2.mp3"],
	"grom3": ["grom3.mp3"],
},
"images": [
	"bgsquare.png",
	"favoicon.png",
	"scrshot.png",
	"temple.png",
	"sprites.png",
	"tower.png",
	"snakesprite.png",
	"coins.png",
	"wallsprite.png"],
};