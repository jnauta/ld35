var soundMap = {
	snakemodesound: ['assets/sound/snakemodesound.mp3', 'assets/sound/snakemodesound.ogg'],
	bgmusic: ['assets/sound/bgmusic.mp3', 'assets/sound/bgmusic.ogg'],
	slide: ['assets/sound/slide.mp3', 'assets/sound/slide.ogg'],
	grom4: ['assets/sound/grom4.mp3', 'assets/sound/grom4.ogg'],
	grom1: ['assets/sound/grom1.mp3', 'assets/sound/grom1.ogg'],
	grom2: ['assets/sound/grom2.mp3', 'assets/sound/grom2.ogg'],
	grom3: ['assets/sound/grom3.mp3', 'assets/sound/grom3.ogg'],
};